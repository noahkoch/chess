require 'colored'
require './board'
require './human'
require './pieces'

class Game
  attr_accessor :players, :my_board

  def initialize(white_player = HumanPlayer.new, black_player = HumanPlayer.new)
    @players = {white: white_player, black: black_player}

    @my_board = Board.new
    my_board.initiate_pieces
    start_game
  end

  def start_game
    puts "Welcome to Chess"
    puts game_loop
    # puts "#{player_color.capitalize} was put in checkmate."
  end

  def game_loop
    loop do
      @players.each do |player_color, player_obj|
        if my_board.check_on_board
          puts "#{my_board.check_on_board.capitalize} is in check"
          if my_board.checkmate?(player_color)
            return "#{player_color.capitalize} was put in checkmate."
          end
        else
          if my_board.checkmate?(player_color)
            return "#{player_color.capitalize} was put in stalemate."
          end
        end

        my_board.render_board
        player_obj.make_move(player_color, my_board)
      end
    end
  end
end

if $0 == __FILE__
  Game.new
end