class Employee
  attr_accessor :title, :boss, :salary
  def initialize

  end

  def calculate_bonus(multiplier)
    bonus = salary * multiplier
  end

end

class Manager < Employee
  attr_accessor :new_employee, :employee_list

  def initialize
    @employee_list = []
  end

  def new_employee=(employee)
    employee.boss = self
    @employee_list << employee
  end

  # def employees_salary
#     employee_salary = 0
#     self.employee_list.each do |employee|
#       employee_salary += employee.salary
#     end
#   end

  def calculate_bonus(multiplier)
    employees_salary * multiplier
  end

  def employees_salary
    # includes the top employee's salary in total
    sum = 0
    if self.is_a?(Manager)
      unless self.employee_list.empty?
        self.employee_list.each do |employee|
          employee.is_a?(Manager) ? sum += employee.employees_salary : sum += employee.salary
        end
      end
      sum += self.salary
    end
    sum
  end
end
# a = Manager.new
# b = Manager.new
# c = Manager.new
# d = Employee.new
# e = Employee.new
# f = Employee.new
#
# a.new_employee = b
# a.new_employee = c
# a.new_employee = d
# b.new_employee = e
# b.new_employee = f
#
# a.salary = 15
# b.salary = c.salary = d.salary = 10
# f.salary = e.salary = 5

