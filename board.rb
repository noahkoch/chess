# encoding: UTF-8
require 'debugger'

class Board
  attr_accessor :pieces, :board, :white_pieces, :black_pieces, :killed_pieces
  def initialize
    @board = Array.new(8){ Array.new(8,"   ")}
    @pieces = []
    @killed_pieces = []
    @black_pieces = {
      :king => " ♔ ",
      :queen => " ♕ ",
      :rook => " ♖ ",
      :bishop => " ♗ ",
      :knight => " ♘ ",
      :pawn => " ♙ "
    }
    @white_pieces = {
      :king => " ♚ ",
      :queen => " ♛ ",
      :rook => " ♜ ",
      :bishop => " ♝ ",
      :knight => " ♞ ",
      :pawn => " ♟ "
    }
  end

  def check?(coord_arr, my_color)
    enemy_moves = opposing_moves(my_color)
    coord_arr.any? {|coord| enemy_moves.include?(coord) }
  end

  def check_on_board
    kings = pieces.select{ |piece| piece.is_a?(King) }
    kings.each do |king_obj|
      if check?([ king_obj.cross_street ], king_obj.color)
        return king_obj.color
      end
    end
    nil
  end

  def opposite_color(color)
    color == :white ? :black : :white
  end

  def checkmate?(my_color)

    possible_moves = 0

    opposing_movements(opposite_color(my_color)).each do |from_to_pair|
      move_from, many_tos = from_to_pair[0], from_to_pair[1]

      many_tos.each do |move_to|
        possible_moves += 1 if valid_to?(move_from, move_to, my_color, false)
      end
    end

    return true if possible_moves.zero?
    nil
  end

  def opposing_moves(my_color)
    enemies = pieces.select{|piece| piece.color != my_color }
    all_enemy_moves = []
    enemies.each do |piece|
      if piece.is_a?(Pawn)
        all_enemy_moves += piece.valid_moves(piece.color)
      else
        all_enemy_moves += piece.valid_moves unless piece.is_a?(King)
      end
    end
    all_enemy_moves
  end

  def opposing_movements(my_color)
    enemies = pieces.select{|piece| piece.color != my_color }
    movements = []
    enemies.each do |piece|
      if piece.is_a?(Pawn)
        movements << [piece.cross_street, piece.valid_moves(piece.color)]
      else
        movements << [piece.cross_street, piece.valid_moves]
      end
    end
    movements
  end

  def update_pieces(from_to_coords, delete_pieces = true, &blk)
    blk ||= Proc.new { |piece| true }
    from_coord, to_coord = from_to_coords[0], from_to_coords[1]
    moving_piece = occupant(from_coord, &blk)

    if occupant(to_coord)
      killed_pieces << occupant(to_coord) if delete_pieces
      pieces.delete(occupant(to_coord)) if delete_pieces
    end
    moving_piece.cross_street = to_coord
  end

  def valid_from?(coord, color)
    piece = occupant(coord)
    return false unless piece
    return false unless piece.color == color
    true
  end

  def valid_to?(from_coord, to_coord, color, delete_pieces = true)
    piece = occupant(from_coord)
    if piece.is_a?(Pawn)
      return false unless piece.valid_moves(color).include?(to_coord)
    else
      return false unless piece.valid_moves.include?(to_coord)
    end
    orig_check = check_on_board
    if orig_check
      update_pieces([from_coord, to_coord], delete_pieces)

      if check_on_board == orig_check
        update_pieces([to_coord, from_coord], delete_pieces) { |piece| piece.color == color }
        return false
      else
        update_pieces([to_coord, from_coord], delete_pieces) { |piece| piece.color == color }
      end
    end
    true
  end

  def initiate_pieces
    8.times do |x|
      pieces << Pawn.new(self,[6,x], :white)
    end
    8.times do |x|
      pieces << Pawn.new(self,[1,x], :black)
    end
    pieces << Queen.new(self,[7,3], :white)
    pieces << Queen.new(self,[0,3], :black)
    pieces << King.new(self,[7,4], :white)
    pieces << King.new(self,[0,4], :black)
    2.times do |x|
      x == 0 ? x = 2 : x = 5
      pieces << Bishop.new(self,[0,x], :black)
      pieces << Bishop.new(self,[7,x], :white)
    end
    2.times do |x|
      x == 0 ? x = 1 : x = 6
      pieces << Knight.new(self,[0,x], :black)
      pieces << Knight.new(self,[7,x], :white)
    end
    2.times do |x|
      x == 0 ? x = 0 : x = 7
      pieces << Rook.new(self,[0,x], :black)
      pieces << Rook.new(self,[7,x], :white)
    end
  end

  def occupant(coord, &blk)
    blk ||= Proc.new { |piece| true }
    piece_on_coord =  pieces.select do |piece|
      piece.cross_street == coord && blk.call(piece)
    end
    piece_on_coord.last ? piece_on_coord.last : nil
  end

  def vacant?(coord)
    board[coord[0]][coord[1]] == '   '
  end

  def prepare_board
    @board = Array.new(8){ Array.new(8,"   ")}
    pieces.each do |piece|
      piece_y = piece.cross_street[0]
      piece_x = piece.cross_street[1]
      piece_name = piece.class.to_s.downcase.to_sym
      if piece.color == :black
        board[piece_y][piece_x] = black_pieces[piece_name]
      elsif piece.color == :white
        board[piece_y][piece_x] = white_pieces[piece_name]
      end
    end
  end

  def render_board
    puts "    #{Array("a".."h").join("  ")}"
    # puts "    #{Array(0..7).join("  ")}"
    prepare_board
    @board.each_with_index do |line,index|
      array = []
      line.each_with_index do |tile, tile_index|

        if index % 2 ==0
          array << ((tile_index % 2 == 0) ? "#{tile.on_red}" : "#{tile}")
        else
          array << ((tile_index % 2 == 0) ? "#{tile}" : "#{tile.on_red}")
        end
      end
      puts " #{(index - 8).abs} #{array.join}"
    end
  end
end