class Piece
  attr_accessor :cross_street, :board, :color, :origin

  def initialize(board, coord, color)
    @board = board
    @cross_street = coord
    @origin = coord
    @color = color
  end

  def is_my_enemy?(other_piece)
    return false if other_piece == nil
    self.color != other_piece.color
  end

  def is_my_color?(other_piece)
    return false if other_piece == nil
    self.color == other_piece.color
  end

  def on_board?(coord)
    coord.all? do |c|
      c.between?(0,7)
    end
  end

  def off_board?(coord)
    !on_board?(coord)
  end

  def straight_lines
    straights = []
    straights << Array(1..7).map { |num| [0, num] }
    straights << Array(1..7).map { |num| [0, -num] }
    straights << Array(1..7).map { |num| [num, 0] }
    straights << Array(1..7).map { |num| [-num, 0] }
    straights.map{|arr| validate_lines(arr)}.flatten(1)
  end

  def diagonal_lines
    diags = []
    diags << Array(1..7).map { |num| [-num, num] } # top_right
    diags << Array(1..7).map { |num| [-num, -num] } # top_left =
    diags << Array(1..7).map { |num| [num, num] } # bottom_right =
    diags << Array(1..7).map { |num| [num, -num] } # bottom_left =
    diags.map{|arr| validate_lines(arr)}.flatten(1)
  end

  def validate_lines(array)
    valid_moves = []

    array.each do |transform|
      new_position = [cross_street[0] + transform[0], cross_street[1] + transform[1]]
      break if off_board?(new_position) || is_my_color?(@board.occupant(new_position))
      valid_moves << new_position
      break if is_my_enemy?(@board.occupant(new_position))
    end
    valid_moves
  end

  def validate_moves(array, &blk)
    # general validation, for most pieces
    blk ||= Proc.new do |coord|
      is_my_enemy?(@board.occupant(coord)) || @board.vacant?(coord)
    end
    valid_moves = []
    array.each do |transform|
      new_position = [cross_street[0] + transform[0], cross_street[1] + transform[1]]
      if on_board?(new_position)
        valid_moves << new_position if blk.call(new_position)
      end
    end
    valid_moves
  end
end

class Pawn < Piece

  def valid_moves(color)
    valid_moves_to_empty(color) + valid_moves_to_filled(color) + valid_first_move(color)
  end

  def valid_moves_to_empty(color)
    if color == :white
      travel_to_empty = [[-1,0]]
    elsif color == :black
      travel_to_empty = [[1,0]]
    end
    validate_moves(travel_to_empty) { |coord| @board.vacant?(coord) }
  end

  def valid_moves_to_filled(color)
    if color == :white
      travel_to_filled = [[-1,-1], [-1,1]]
    elsif color == :black
      travel_to_filled = [[1,-1], [1,1]]
    end
    validate_moves(travel_to_filled) do |coord|
      is_my_enemy?(@board.occupant(coord))
    end
  end

  def valid_first_move(color)
    valid_moves = []
    if color == :white
      travel_from_first = [-2,0]
    elsif color == :black
      travel_from_first = [2,0]
    end
    if cross_street == origin
      new_position = [cross_street[0] + travel_from_first[0], cross_street[1] + travel_from_first[1]]
      valid_moves << new_position
    end
    valid_moves
  end
end

class King < Piece
  def valid_moves
    travel = [[0,1],[1,0],[1,1],[-1,-1],[-1,0],[0,-1],[-1,1],[1,-1]]
    threatening_tiles = board.opposing_moves(color)
    poss_moves = validate_moves(travel)
    difference = poss_moves - threatening_tiles
    difference
  end
end

class Knight < Piece
  def valid_moves
    travel = [[-1,2],[1,2],[2,1],[2,-1],[1,-2],[-1,-2],[-2,-1],[-2,1]]
    validate_moves(travel)
  end
end

class Rook < Piece
  def valid_moves
    straight_lines
  end
end

class Bishop < Piece
  def valid_moves
    diagonal_lines
  end
end

class Queen < Piece
  def valid_moves
    diagonal_lines + straight_lines
  end
end