# require 'regex'

class HumanPlayer

  def initialize
  end

  def get_move(color, board_obj)
    puts "#{color.capitalize}, Which piece would you like to move? Ex: a1"
    raw_input1 = gets.chomp

    unless raw_input1 =~ /[a-h][1-8]/
      raise ArgumentError.new("You must enter a letter (a to h) followed by a number (1 to 8)")
    end

    from_coord = parse_move(raw_input1)

    if board_obj.vacant?(from_coord)
      raise ArgumentError.new("You don't have a piece there, that's an empty spot!")
    elsif board_obj.occupant(from_coord).color != color
      raise ArgumentError.new("You can't move your opponent's piece!")
    end

    piece_name = board_obj.occupant(from_coord).class

    puts "#{color.capitalize}, Where would you like to move your #{piece_name}?"
    
    raw_input2 = gets.chomp

    unless raw_input2 =~ /[a-h][1-8]/
      raise ArgumentError.new("You must enter a letter (a to h) followed by a number (1 to 8)")
    end

    to_coord = parse_move(raw_input2)

    unless board_obj.valid_to?(from_coord, to_coord, color)
      raise ArgumentError.new("You cannot move #{piece_name} #{raw_input1} to there!")
    end

    move = [from_coord, to_coord]
  end

  def make_move(color, board_obj)
    begin
      move = get_move(color, board_obj)
    rescue ArgumentError => e
      puts e.message
      retry
    end

    board_obj.update_pieces(move)
  end

  def parse_move(move_a1)
    split = move_a1.split(//)
    split[0] = "abcdefgh".index(split[0])
    split[1] = (split[1].to_i - 8).abs
    split.reverse
  end
end